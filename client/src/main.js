// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';

// importing vuetify

import vuetify from 'vuetify';
import '@mdi/font/css/materialdesignicons.css'; // Ensure you are using css-loader
import 'vuetify/dist/vuetify.min.css';
const options = {
  theme: {
    light: true,
    themes: {
      light: {
        primary: '#00b2ba',
        secondary: '#3f51b5',
        accent: '#f44336',
        error: '#e91e63',
        warning: '#8bc34a',
        info: '#03a9f4',
        success: '#4caf50',
        themeColor: '#00b2ba',
        bclinkcolor: '#e91e63',
        cancelbuttoncolor: '#FFCCBC',
        tabcolor: '#FFCCBC'
      },
      dark: {}
    }
  },
  icons: {
    iconfont: 'mdi' // default - only for display purposes
  },
  customProperties: true // adding new scoped style
}
Vue.use(vuetify)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app', // the application id declared in the index.html file
  // no need to write previous version
  vuetify: new vuetify(options),
  router,
  components: { App },
  template: '<App/>'
})
