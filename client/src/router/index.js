import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/components/HelloWorld';
import Contact from '@/components/Contact';
import AddNewToDo from '@/views/todos/AddNewTodo';
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    // Adding New Todo Router
    {
      path: '/todo/add',
      name: 'AddNewToDo',
      component: AddNewToDo
    },
    {
      path: '/contact',
      name: 'Contact',
      component: Contact
    }
  ]
})
