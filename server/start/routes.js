"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

// importing the Todo Model
const Todo = use("App/Models/Todo");

// getting all the todos
Route.get("/api/todo/all", async function ({ response }) {
  const todos = await Todo.query().fetch();
  console.log(todos.toJSON());
  return todos.toJSON();
});

// adding new todo
Route.post("/api/todo/add", function ({ request }) {
  //console.log(request.all());
  // getting all the request params
  const { title } = request.input("newTodo");

  const newTodo = new Todo();
  newTodo.name = title;
  newTodo.todo_time = "2019-08-20";
  newTodo.status = 0;
  newTodo.save();
  return newTodo;
});

// deleting todo
Route.post("/api/todo/delete", async function ({ request }) {
  // console.log(request.all());
  // getting all the request params
  const id = request.input("id");
  //return id;
  // checking todo is exist or not before deleting
  const checkTodo = await Todo.query().where("id", id).first();
  if (!checkTodo) return "Resouce Missing";
  // to do exist so delete it
  await checkTodo.delete();
  return "success";
});

// editing todo
Route.post("/api/todo/edit", async function ({ request }) {
  // console.log(request.all());
  // getting all the request params
  const id = request.input("id");
  //return id;
  const editTodo = await Todo.query().where("id", id).first();
  console.log(request.all());
  if (!checkTodo) return "Resouce Missing";
  // if todo exist then edit it
  await editTodo.save("id");
  return "success";
});