"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Todo extends Model {
  /** Setting the Database table name */
  static get table() {
    return "todos";
  }
}

module.exports = Todo;
