# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

1. ToDo Apps Written into Vue/Adonis/MySQL 
2. The infrastructure code are written AWS CFN for Frontend/Backend/Database  
3. Deployment code is written into Terraform and finally deploy to AWS Cloud

### How do I get set up? ###
# Adonis API application

This is the boilerplate for creating an API server in AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Authentication
3. CORS
4. Lucid ORM
5. Migrations and seeds

# client

> Portal to handle the front end section

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick --api-only
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```

## Serving the application
```
adonis serve --dev
```

## Generate API Key
```
adonis key:generate
```

## Install Adonish

npm i -g @adonisjs/cli
adonis key:generate

npm install sqlite3 --save
adonis migration:run
adonis serve --dev
